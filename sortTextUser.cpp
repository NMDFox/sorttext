#include <string>
#include <vector>
#include <iostream>
#include <locale>
#include <fstream>

// Structure used to hold the letter char, and the number of occurrences found in the string.
typedef struct
{
  char letter;
  int  occurences;
} charDetails;

// Main function, will always return zero
int main(int argc, char **argv)
{
  charDetails tempChar;
  std::vector<charDetails> chars;
  bool found = false;
  std::string testString;
  std::ofstream outputFile;

  // Use static string
  testString = "Imagine an imaginary menagerie manager managing an imaginary menagerie";

  // Loop through the string and add each occurence to a running total for each char
  for( int i = 0; i < testString.length(); i++)
  {
    // Validate each char here (Alphabet letters only)
    if( isalpha(testString.at(i)) )
    {
      for( int j = 0; j < chars.size(); j++)
      {
        // Convert to lower case to prevent duplicate entries into vector
        if( chars[j].letter == std::tolower(testString.at(i)))
        {
          chars[j].occurences ++;
          // Found! move on to the next char
          found = true;
        }
      }

      // We have got to the end of the existing vector, but didnt find the char, add it here and
      // increment the counter by 1, we will only need to enter this section once per new char.
      if(found == false)
      {
        tempChar.letter = std::tolower(testString.at(i));
        tempChar.occurences = 1;

        chars.push_back(tempChar);
      }

      // Set this bool to false before checking the next char in the string.
      found = false;
    }
  }


  // Sort vector using an insertion sort algorithm
  if(chars.size() > 1)
  {
    int size = chars.size();
    for(int i = 1; i < size; i++)
    {
      int j = i - 1;
      charDetails key = chars[i];
      while(j >= 0 && chars[j].occurences < key.occurences)
      {
        chars[j+1] = chars[j];
        j--;
      }
      chars[j+1] = key;
    }
  }

  // Output the results to a text file in /tmp
  outputFile.open("/tmp/sortText.csv");
  outputFile << "Letter," << "Occurences\n";

  // Print Contents of vector to terminal and also to output file
  for(int k = 0; k < chars.size(); k++)
  {
    std::cout << "Letter: " << chars[k].letter << " Occurences: " << chars[k].occurences << "\n";
    outputFile << chars[k].letter << "," << chars[k].occurences << "\n";
  }

  outputFile.close();

  return 0;
}